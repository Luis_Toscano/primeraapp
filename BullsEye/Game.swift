//
//  Game.swift
//  BullsEye
//
//  Created by LuisT on 8/5/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import Foundation

/* Moviendo logica del negocio al modelo */
class Game {
    var target = 0;
    var score = 0;
    var roundGame = 1;
    
    var isWinner:Bool {
        return score > 100;
    }
    
    func restartGame () {
        target = generateGameNumber();
        score = 0;
        roundGame = 1;
    }
    
    func generateGameNumber() -> Int {
        let newNumber:Int = Int(arc4random_uniform(100) + 1);
        return newNumber;
    }
    
    func play (_ guessedNumber:Int) {
        switch guessedNumber {
            case target:
                score = score + 100;
            case (target - 2) ... (target + 2):
                score = score + 50;
            case (target - 10) ... (target + 10):
                score = score + 5;
            default:
                score = score - 10;
            
        }
    }
    
    
    
    
}
