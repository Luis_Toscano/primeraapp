//
//  ViewController.swift
//  BullsEye
//
//  Created by LuisT on 24/4/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var roundCounterLabel: UILabel!
    @IBOutlet weak var gameNumberLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    var game:Game = Game();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        game.restartGame ();
    }

//    func restartGame () {
//        roundCounterLabel.text = "1";
//        scoreLabel.text = "0";
//        gameNumberLabel.text = generateGameNumber();
//    }
    
    @IBAction func restartButton(_ sender: UIButton) {
        game.restartGame();
    }
    
//    func generateGameNumber() -> String {
//        let newNumber:Int = Int(arc4random_uniform(100) + 1);
//        return String(newNumber);
//    }
    
//    func game (_ guessedNumber:Int) {
//        let gameNumber = Int(gameNumberLabel.text!);
//
//        switch guessedNumber {
//            case gameNumber:
//                var actualScore = Int(scoreLabel.text!);
//                actualScore = actualScore! + 100;
//                scoreLabel.text = String(actualScore!);
//            case (gameNumber! - 2) ... (gameNumber! + 2):
//                var actualScore = Int(scoreLabel.text!);
//                actualScore = actualScore! + 50;
//                scoreLabel.text = String(actualScore!);
//            case (gameNumber! - 10) ... (gameNumber! + 10):
//                var actualScore = Int(scoreLabel.text!);
//                actualScore = actualScore! + 5;
//                scoreLabel.text = String(actualScore!);
//            default:
//                var actualScore = Int(scoreLabel.text!);
//                actualScore = actualScore! - 25;
//                scoreLabel.text = String(actualScore!);
//
//        }
//
//        gameNumberLabel.text = generateGameNumber();
//        roundCounterLabel.text = String(Int(roundCounterLabel.text!)! + 1);
//    }
    
    // CONECTADO COMO OUTLET, PARA ACCEDER A SUS PROPIEDADES
    // CONECTADO COMO ACTION PARA ACCEDER A SUS EVENTOS.
    @IBAction func guessedNumberSlider(_ sender: UISlider) {
        game.play(Int(sender.value));
        
    }
    
    @IBAction func secretPressed(_ sender: Any) {
        let currentScore = Int(scoreLabel.text!);
        if (currentScore! >= 100) {
            performSegue(withIdentifier: "toSecretRoomSegue", sender: self)
        }
    }
    
    // IMPLEMENTAR UNA FUNCION PARA ACTUALIZAR LAS ETIQUETAS EN EL FRONTEND.
    
}

